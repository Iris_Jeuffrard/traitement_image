# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 16:20:21 2021

@author: iris jeuffrard
"""


import numpy as np
from PIL import Image, ImageQt
import scipy.ndimage.filters as sc

from PyQt5 import QtWidgets, QtGui


def convert_gray(img):
    return np.uint8(np.mean(img, axis=-1))
   
def gaussian(img, sigma=3):
    gaussian = sc.gaussian_filter(img, sigma)
    return gaussian


if __name__ == "__main__":
   
    # generate data
    table = Image.open("bird.jpg") 
    table =  np.array(table)
    
    # process
#    gray = convert_gray(table)
    gauss = gaussian(table, 5)
    
    # convert data to QImage using PIL
    img = Image.fromarray(gauss, mode='RGB')
    qt_img = ImageQt.ImageQt(img)
    
    # show in  Qt interface 
    app = QtWidgets.QApplication([])
    w = QtWidgets.QLabel()
    w.setPixmap(QtGui.QPixmap.fromImage(qt_img))
    w.show()
    app.exec()