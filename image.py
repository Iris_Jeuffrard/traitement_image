#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""

from PyQt5.QtGui import QImage, QPixmap

import numpy as np
import scipy.ndimage.filters as sc
from PIL import Image, ImageQt

class MyImage():

    def __init__(self, path):
        
        self.path = path
        self.Qimage = QImage(path)
        self.Qpixmap = QPixmap(self.Qimage)

        self.array = np.array(Image.open(self.path))
        
#    def updatePixmap(self):
#        """Met à jour le QPixmap à partir de l'image"""
#        
#        #-- chargement du QImage dans le QPixmap
#        self.pixmap = QPixmap(self.image)
#        #-- affichage du QPixmap dans QLabel
#        self.setPixmap(self.pixmap)
        
    def reset_Image(self):
        self.Qimage = QImage(self.path)
        self.Qpixmap = QPixmap(self.Qimage)
        
    def convert_nparray_to_QPixmap(self,array,mode='RGB'):
        # convert data to QImage using PIL
        imgPIL = Image.fromarray(array, mode=mode)
        self.Qimage = ImageQt.ImageQt(imgPIL)
        self.Qpixmap = QPixmap.fromImage(self.Qimage)
    
