<h1 align="center">Traitements d'image</h1>

***
<h1 align="center">PROJET EN COURS !!!!!!</h1>

***

## Table des matières

-   [À propos](#about)
-   [Installation](#installation)
-   [Tutoriel](#tutoriel)
-   [Documentation](#documentation)


## À propos

Logiciel de traitement d'image : filtres, détection de contours, classification...

## Installation

...

## Tutoriel

### Lancer l'application

Pour lancer l'application, exécutez le fichier `interface.py`.

### Ouvrir une image

Le logiciel traite une image qu'il faudra renseigner. Pour cela, cliquez sur `Fichier > Ouvrir une image` et sélectionnez l'image à traiter.

### Interface

L'interface se compose d'un menu avec les onglets et leurs raccourcis clavier suivants :

* Fichier
  * Ouvrir une image (crtl+O)
  * Enregistrer sous
  * Imprimer (crtl+P)      
  * Quitter (crtl+Q)
* Édition
  * ...
* Vue
  * Zoom + (crtl++)
  * Zoom - (crtl+-)
  * Taille normale (crtl+S)
  * Adapter à la fenêtre (crtl+F)
* Aide
  * À propos
  * À propos de Qt

## Documentation

Le logiciel a été développé en python et PyQt (documentation [ici](https://doc.qt.io/qtforpython-5/))
sous les versions suivantes :

![Python](https://img.shields.io/static/v1?label=Python&message=3.8&color=yellow)
![PyQt](https://img.shields.io/static/v1?label=PyQt&message=5.11.3&color=green)
![Numpy](https://img.shields.io/static/v1?label=Numpy&message=1.20.1&color=blue)


* `interface.py` implémente la classe de l'interface principale.


## Auteur

- [Iris Jeuffrard (@Iris_Jeuffrard)](https://gitlab.com/Iris_Jeuffrard)
