#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 10:32:24 2022

@author: iris jeuffrard
"""

from PyQt5.QtWidgets import QDoubleSpinBox, QWidget, QPushButton, QGridLayout, QLabel
from PyQt5.QtCore import Qt


class PopupPoint(QWidget):    
    """Fenêtre pour saisir les coordonnées des points terrain remarquables (étape 2-3)."""
    
    def __init__(self, pt, parent):
        """
        Parameters
        ----------
        :param [QPoint.x(),Qpoint.y()] pt: liste des coordonnées i et j du point saisi (position de la souris)
        :param QMainWindow parente: fenêtre principale parente
        """
        
        QWidget.__init__(self)
        self.setWindowTitle("Saisir les points terrains")
        self.setWindowModality(Qt.ApplicationModal)
        self.parent = parent
        self.pos = [pt,0]       # coordonnées image & terrain [[i,j],[X,Y,Z]]

        self.gridLayout = QGridLayout()
        self.coordLabel = QLabel("Coordonées X Y Z (m) :")
        self.gridLayout.addWidget(self.coordLabel, 0, 0)
    
        #-- champ de coordonnées X
        self.q1 = QDoubleSpinBox()
        self.q1.setRange(-9999.999,9999.999)
        self.q1.setDecimals(3)        
        self.gridLayout.addWidget(self.q1, 0, 1)

        #-- champ de coordonnées Y
        self.q2 = QDoubleSpinBox()
        self.q2.setRange(-9999.999,9999.999)
        self.q2.setDecimals(3)
        self.gridLayout.addWidget(self.q2, 0, 2)
        
        #-- champ de coordonnées Z
        self.q3 = QDoubleSpinBox()
        self.q3.setRange(-9999.999,9999.999)
        self.q3.setDecimals(3)
        self.gridLayout.addWidget(self.q3, 0, 3)
    
    
        self.button = QPushButton("Save")
        self.button.setShortcut("Return")      
        self.gridLayout.addWidget(self.button,1,3)
        self.button.clicked.connect(self.save)

        self.setLayout(self.gridLayout)

    def save(self):
        """
        Récupère les coordonnées terrain saisies par l'opérateur.
        Appelle la fonction d'affichage du point dans la fenêtre principale .       
        """
        self.pos[1] = [self.q1.value(), self.q2.value(), self.q3.value()]
        self.parent.createListPointStep3()
        self.button.setDisabled(True)
        self.close()
