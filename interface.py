#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 13:46:55 2021

@author: iris jeuffrard

"""

from image import MyImage
import traitements
from PIL import Image, ImageQt

import sys
from PyQt5.QtPrintSupport import QPrintDialog, QPrinter
from PyQt5.QtWidgets import (QApplication, QGroupBox, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QGridLayout, 
                             QDialog, QFileDialog, QLabel, QMainWindow, QMessageBox, QScrollArea, QSizePolicy)
from PyQt5.QtGui import (QIcon, QGuiApplication, QImage, QImageReader, QImageWriter, QKeySequence, QPalette, QPainter, QPixmap)
from PyQt5.QtCore import Qt, QSize, QDir, QStandardPaths,QByteArray


ABOUT = """Logiciel de traitement d'image"""


class MainWindow(QMainWindow):
    """Fenêtre principale de l'application."""

    def __init__(self, parent=None):
        super().__init__(parent)
        
        self.setWindowTitle("Traitement d'image")
        self.setupUi()


    def setupUi(self):
        """Crée l'interface."""
        
        self.createActions()
        self.createMainUi()
        self.resize(QGuiApplication.primaryScreen().availableSize() * 3 / 5)
        
#         import de l'image pour test plus facile - à supprimer
        
        self.image = MyImage("bird.jpg")
        self._set_image(self.image.Qpixmap)
  
    
    def createActions(self):
        """Crée la barre de menu (Fichier, Édition, Vue, Aide) et les actions du menu."""
        
        file_menu = self.menuBar().addMenu("&Fichier")

        self._open_act = file_menu.addAction("&Ouvrir une image")
        self._open_act.triggered.connect(self._open)
        self._open_act.setShortcut(QKeySequence.Open)

        self._save_as_act = file_menu.addAction("&Enregistrer sous")
        self._save_as_act.triggered.connect(self._save_as)
        self._save_as_act.setEnabled(False)

        self._print_act = file_menu.addAction("Imprimer")
        self._print_act.triggered.connect(self._print_)
        self._print_act.setShortcut(QKeySequence.Print)
        self._print_act.setEnabled(False)

        file_menu.addSeparator()

        self._exit_act = file_menu.addAction("&Quitter")
        self._exit_act.triggered.connect(self.close)
        self._exit_act.setShortcut("Ctrl+Q")

        edit_menu = self.menuBar().addMenu("Édition")

        self._black_and_white_act = edit_menu.addAction("Niveaux de gris")
        self._black_and_white_act.triggered.connect(self.black_and_white)

        self._gaussian_act = edit_menu.addAction("filtre Gaussien")
        self._gaussian_act.triggered.connect(self.gaussian_filter)


        view_menu = self.menuBar().addMenu("&Vue")

        self._zoom_in_act = view_menu.addAction("Zoom +")
        self._zoom_in_act.setShortcut(QKeySequence.ZoomIn)
        self._zoom_in_act.triggered.connect(self._zoom_in)
        self._zoom_in_act.setEnabled(False)

        self._zoom_out_act = view_menu.addAction("Zoom -")
        self._zoom_out_act.triggered.connect(self._zoom_out)
        self._zoom_out_act.setShortcut(QKeySequence.ZoomOut)
        self._zoom_out_act.setEnabled(False)

        self._normal_size_act = view_menu.addAction("&Taille normale")
        self._normal_size_act.triggered.connect(self._normal_size)
        self._normal_size_act.setShortcut("Ctrl+S")
        self._normal_size_act.setEnabled(False)

        view_menu.addSeparator()

        self._fit_to_window_act = view_menu.addAction("Adapter à la fenêtre")
        self._fit_to_window_act.triggered.connect(self._fit_to_window)
        self._fit_to_window_act.setEnabled(False)
        self._fit_to_window_act.setCheckable(True)
        self._fit_to_window_act.setShortcut("Ctrl+F")

        help_menu = self.menuBar().addMenu("Aide")

        about_act = help_menu.addAction("À propos")
        about_act.triggered.connect(self._about)
        about_qt_act = help_menu.addAction("À propos de Qt")
        about_qt_act.triggered.connect(QApplication.aboutQt)


    #------------------------- Fonctions actions Fichier ---------------------# 
    
    def _open(self):
        """Ouvre une fenêtre de dialogue pour importer une image."""
        
        dialog = QFileDialog(self, "importer une image")

        self._initialize_image_filedialog(dialog, QFileDialog.AcceptOpen)
        while (dialog.exec() == QDialog.Accepted
               and not self.load_file(dialog.selectedFiles()[0])):
            pass
        
        
    def load_file(self, fileName):
        """Charge l'image à partir du chemin renseigné par l'opérateur."""
        
        self.image = Image(fileName)
        reader = QImageReader(fileName)
        reader.setAutoTransform(True)

        native_filename = QDir.toNativeSeparators(fileName)
        
        if self.image.Qimage.isNull():
            error = reader.errorString()
            QMessageBox.information(self, QGuiApplication.applicationDisplayName(),
                                    f"Cannot load {native_filename}: {error}")
            return False
        
        self._set_image(self.image.Qpixmap)
        self.setWindowFilePath(fileName)

#        message = f'Image "{native_filename}"'
#        self.statusBar().showMessage(message)
        
        return True


    def _set_image(self, pixmap):
        """Charge l'image dans le label."""

        self.label.setPixmap(pixmap)
        
        self._scale_factor = 1.0

        self._scroll_area.setVisible(True)
        self._print_act.setEnabled(True)
        self._fit_to_window_act.setEnabled(True)
        self._update_actions()

        if not self._fit_to_window_act.isChecked():
            self.label.adjustSize()

    
    def _save_as(self):
        """Ouvre une fenêtre de dialogue pour enregistrer l'image."""
        
        dialog = QFileDialog(self, "Enregistrer l'image sous")
        self._initialize_image_filedialog(dialog, QFileDialog.AcceptSave)
        while (dialog.exec() == QDialog.Accepted
               and not self._save_file(dialog.selectedFiles()[0])):
            pass


    def _save_file(self, fileName):
        """Sauvegarde l'image selon le chemin renseigné par l'opérateur."""
        
        writer = QImageWriter(fileName)
        native_filename = QDir.toNativeSeparators(fileName)
        if not writer.write(self.image.Qimage):
            error = writer.errorString()
            message = f"Cannot write {native_filename}: {error}"
            QMessageBox.information(self, QGuiApplication.applicationDisplayName(),
                                    message)
            return False
        self.statusBar().showMessage(f'Wrote "{native_filename}"')
        return True


    def _print_(self):
        """Ouvre une fenêtre de dialogue pour imprimer l'image."""
        
        printer = QPrinter()
        dialog = QPrintDialog(printer, self)
        if dialog.exec() == QDialog.Accepted:
            painter = QPainter(printer)
            pixmap = self.label.pixmap()
            rect = painter.viewport()
            size = pixmap.size()
            size.scale(rect.size(), Qt.KeepAspectRatio)
            painter.setViewport(rect.x(), rect.y(), size.width(), size.height())
            painter.setWindow(pixmap.rect())
            painter.drawPixmap(0, 0, pixmap)
            painter.end()


    #-------------------------- Fonctions actions Vue ------------------------# 

    
    def _zoom_in(self):
        """Zoom sur l'image d'un facteur 1,25."""
        self._scale_image(1.25)

    
    def _zoom_out(self):
        """Dé-zoom sur l'image d'un facteur 0,8."""
        self._scale_image(0.8)


    def _scale_image(self, factor):
        """Met à l'échelle l'image, ajuste le label et les scrollbars."""
        
        self._scale_factor *= factor
        new_size = self._scale_factor * self.image.Qpixmap.size()
        self.label.resize(new_size)

        self._adjust_scrollbar(self._scroll_area.horizontalScrollBar(), factor)
        self._adjust_scrollbar(self._scroll_area.verticalScrollBar(), factor)

        self._zoom_in_act.setEnabled(self._scale_factor < 15.0)
        self._zoom_out_act.setEnabled(self._scale_factor > 0)


    def _adjust_scrollbar(self, scrollBar, factor):
        """Ajuste les scrollBar au facteur de zoom."""
        
        pos = int(factor * scrollBar.value()
                  + ((factor - 1) * scrollBar.pageStep() / 2))
        scrollBar.setValue(pos)
        
    
    def _normal_size(self):
        """Restaure la taille originale de l'image."""
        self.label.adjustSize()
        self._scale_factor = 1.0

    
    def _fit_to_window(self):
        """Adapte l'image à la taille de la fenêtre."""

        fit_to_window = self._fit_to_window_act.isChecked()
        self._scroll_area.setWidgetResizable(fit_to_window)
        if not fit_to_window:
            self._normal_size()
        self._update_actions()


    #------------------------ Fonctions Aide et Autre ------------------------# 


    def _about(self):
        """Affiche le message 'À propos'."""
        QMessageBox.about(self, "À propos de Photogrammétrie mono-image", ABOUT)


    def _initialize_image_filedialog(self, dialog, acceptMode):
        """Implémente les boites de dialogue."""
        
        if self._first_file_dialog:
            self._first_file_dialog = False
            locations = QStandardPaths.standardLocations(QStandardPaths.PicturesLocation)
            directory = locations[-1] if locations else QDir.currentPath()
            dialog.setDirectory(directory)

        mime_types = [m.data().decode('utf-8') for m in QImageWriter.supportedMimeTypes()]
        mime_types.sort()

        dialog.setMimeTypeFilters(mime_types)
        dialog.selectMimeTypeFilter("image/jpeg")
        dialog.setAcceptMode(acceptMode)
        if acceptMode == QFileDialog.AcceptSave:
            dialog.setDefaultSuffix("jpg")
    
    
    def _update_actions(self):
        """Active les actions dès qu'il y a une image."""
        
        has_image = not self.image.Qimage.isNull()
        self._save_as_act.setEnabled(has_image)
        enable_zoom = not self._fit_to_window_act.isChecked()
        self._zoom_in_act.setEnabled(enable_zoom)
        self._zoom_out_act.setEnabled(enable_zoom)
        self._normal_size_act.setEnabled(enable_zoom)


    #------------------------- Fonctions actions Edition ---------------------# 

    def black_and_white(self):
        """black and white."""
        gray = traitements.convert_gray(self.image.array)
        self.image.convert_nparray_to_QPixmap(gray,mode='L')
        self.label.setPixmap(self.image.Qpixmap)

    def gaussian_filter(self):
        """filtre gaussien."""
        table = traitements.gaussian(self.image.array, sigma=3)
        self.image.convert_nparray_to_QPixmap(table,mode='RGB')
        self.label.setPixmap(self.image.Qpixmap)

        
    #------------------------------- Main UI ---------------------------------# 
    
    def createMainUi(self):
        """Crée l'ensemble des composants de l'interface."""

        self.centralWidget = QWidget()
        self.mainLayout = QVBoxLayout()
        self.centralWidget.setLayout(self.mainLayout)

        #-- VIEW COMPONENTS - Scroll Area
        
        self.imageLayout = QVBoxLayout() 
        
        self._scale_factor = 1.0
        self._first_file_dialog = True
        self.label = QLabel(self)
        self.label.setBackgroundRole(QPalette.Base)
        self.label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.label.setScaledContents(True)
        
        self._scroll_area = QScrollArea()
        self._scroll_area.setBackgroundRole(QPalette.Dark)
        self._scroll_area.setWidget(self.label)
        self._scroll_area.setVisible(False)

        self.imageLayout.addWidget(self._scroll_area)
        self.mainLayout.addLayout(self.imageLayout)
        
        ###-- Group Box Édition
        
        self.groupBox = QGroupBox("Édition")

        self.toolGridLayout = QGridLayout()

        self.resetIcon = QIcon("img/trash-alt-solid.svg")
        self.resetButton = QPushButton("")
        self.resetButton.setIcon(self.resetIcon)
        self.resetButton.setFixedSize(QSize(50, 50))
        self.resetButton.clicked.connect(self.reset)
        self.resetButton.setDisabled(False)
 
        self.backIcon = QIcon("img/reply-solid.svg")
        self.backButton = QPushButton("")
        self.backButton.setIcon(self.backIcon)
        self.backButton.setFixedSize(QSize(50, 50))
        self.backButton.clicked.connect(self.back)
        self.backButton.setDisabled(False)

        self.nextIcon = QIcon("img/share-solid.svg")
        self.nextButton = QPushButton("")
        self.nextButton.setIcon(self.nextIcon)
        self.nextButton.setFixedSize(QSize(50, 50))
        self.nextButton.clicked.connect(self.cancelBack)
        self.nextButton.setDisabled(False)
        

        self.toolGridLayout.addWidget(self.resetButton, 0, 0)
        self.toolGridLayout.addWidget(self.backButton, 0, 1)
        self.toolGridLayout.addWidget(self.nextButton, 0, 2)


        self.groupBox.setLayout(self.toolGridLayout) 
        self.mainLayout.addWidget(self.groupBox)
        
        self.setCentralWidget(self.centralWidget)
        


    #--------------------------- Fonctions outils ----------------------------# 
    
    def reset(self):
        self.image.reset_Image()
        self.label.setPixmap(self.image.Qpixmap)

    def back(self):
        print("back")

    def cancelBack(self):
        print("cancel back")

    def save(self):
        print("save")


if __name__=="__main__":
    
    app = QApplication.instance() 
    if not app:
        app = QApplication(sys.argv)
        
    mainWindow = MainWindow()
    mainWindow.show()
    
    app.exec_()

